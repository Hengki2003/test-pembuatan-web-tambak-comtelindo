<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PondController@index');
Route::get('/{fishpond}', 'PondController@pondLocation');
Route::post('/parameter/{fishpond}', 'PondController@updateParameter');
Route::get('/{fishpond}/history', 'PondController@history');
Route::get('/{history}/history/detail', 'PondController@detailHistory');
Route::post('/{history}/history/export', 'PondController@export');
Route::post('/history/import', 'PondController@storeImport');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
