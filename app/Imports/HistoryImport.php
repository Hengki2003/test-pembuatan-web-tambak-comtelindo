<?php

namespace App\Imports;

use App\Models\DetailHistory;
use App\Models\History;
use App\Models\Fishpond;
use App\Models\Site;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class HistoryImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $location = explode(', ', $row['lokasi']);
        $site = Site::where('location', $location[0])->where('city', $location[1])->first();
        $fishpond = Fishpond::where('name', $row['nama_tambak'])->where('site_id', $site->id)->first();

        if ($fishpond != null) {
            $history = History::where('fishpond_id', $fishpond->id)->where('date', $row['tanggal'])->first();
            // dd($row);
            if ($history != null) {
                if (DetailHistory::where('history_id', $history->id)->where('time', $row['jam'])->first() == null) {
                    return new DetailHistory([
                        'history_id' => $history->id,
                        'time' => $row['jam'],
                        'ph' => $row['ph'],
                        'suhu' => $row['suhu'],
                        'oxygen' => $row['dissolved_oxygen_do'],
                    ]);
                }
            } else {
                $history = History::create([
                    'fishpond_id' => $fishpond->id,
                    'date' => $row['tanggal'],
                ]);

                return new DetailHistory([
                    'history_id' => $history->id,
                    'time' => $row['jam'],
                    'ph' => $row['ph'],
                    'suhu' => $row['suhu'],
                    'oxygen' => $row['dissolved_oxygen_do'],
                ]);
            }
        } else {
            return new DetailHistory([

            ]);
        }
    }
}
