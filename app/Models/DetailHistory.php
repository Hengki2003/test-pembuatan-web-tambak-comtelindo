<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'history_id',
        'time',
        'ph',
        'suhu',
        'oxygen',
    ];

    public function history()
    {
        return $this->belongsTo('App\Models\History');
    }
}
