<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fishpond extends Model
{
    use HasFactory;

    public function history()
    {
        return $this->hasMany('App\Models\history');
    }

    public function site()
    {
        return $this->belongsTo('App\Models\Site');
    }
}
