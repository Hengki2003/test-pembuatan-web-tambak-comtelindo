<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;

    protected $fillable = [
        'fishpond_id',
        'date',
    ];

    public function fishpond()
    {
        return $this->belongsTo('App\Models\Fishpond');
    }

    public function detailHistory()
    {
        return $this->hasMany('App\Models\DetailHistory');
    }
}
