<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    use HasFactory;

    protected $fillable = [
        'fishpond_id',
        'ph',
        'suhu',
        'oxygen'
    ];

    public function fishpond()
    {
        return $this->belongsTo('App\Models\Fishpond');
    }
}
