<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\FishPond;
use App\Models\History;
use App\Models\DetailHistory;
use App\Models\Parameter;

use Carbon\Carbon;
use App\Exports\HistoryExport;
use App\Imports\HistoryImport;
use Excel;

class PondController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $location = "Balikpapan";
        $fishponds = Fishpond::all();

        return view('fishpond.index', compact('fishponds'));
    }

    public function pondLocation($id)
    {
        $fishponds = Fishpond::all();
        $pond = Fishpond::where('id', $id)->first();

        $history = History::where('date', date('Y-m-d'))->where('fishpond_id', $id)->first();
        if ($history == null) {
            $detailsHistory = collect([]);
        } else {
            $detailsHistory = DetailHistory::where('history_id', $history->id)->orderBy('id', 'DESC')->get()->sortBy('time')->chunk(4)[0];
        }

        $parameter = Parameter::where('fishpond_id', $id)->first();

        return view('fishpond.pondLocation', compact('fishponds', 'detailsHistory', 'parameter', 'pond'));
    }

    public function history(Request $request, Fishpond $fishpond)
    {
        $id = $fishpond->id;

        $fishponds = Fishpond::all();

        if ($request->ajax()) {
            if ($request->data1 != null && $request->data2 != null) {
                $data = History::whereBetween('date', [Carbon::parse($request->data1)->format('Y-m-d'), Carbon::parse($request->data2)->format('Y-m-d')])->where('fishpond_id', $fishpond->id)->get();

            } else {
                $data = History::where('fishpond_id', $fishpond->id)->get();
            }

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('name', function($data){
                    $name = $data->fishpond->name;
                    return $name;
                })
                ->addColumn('ph', function($data){
                    $ph = $data->detailHistory->avg('ph');
                    return number_format($ph, 2);
                })
                ->addColumn('suhu', function($data){
                    $suhu = $data->detailHistory->avg('suhu');
                    return number_format($suhu, 2);
                })
                ->addColumn('oxygen', function($data){
                    $oxygen = $data->detailHistory->avg('oxygen');
                    return number_format($oxygen, 2);
                })
                ->addColumn('action', function($data){
                    $button = "
                    <div style=\"width: 200px\">
                        <a href=\"/$data->id/history/detail\" class=\"btn btn-primary\">Detail</a>
                    </div>";
                    return $button;
                })
                ->rawColumns(['name', 'ph', 'suhu', 'oxygen', 'action'])
                ->make(true);
        }

        $tanggal1 = null;
        $tanggal2 = null;

        return view('fishpond.history', compact('fishponds', 'id', 'tanggal1', 'tanggal2'));
    }

    public function detailHistory(History $history)
    {
        if ($history == null) {
            $detailsHistory = collect([]);
        } else {
            $detailsHistory = $history->detailHistory;
        }

        $fishpond = $history->fishpond;

        $parameter = Parameter::where('fishpond_id', $history->fishpond_id)->first();

        return view('fishpond.detail-history', compact('history', 'detailsHistory', 'parameter', 'fishpond'));
    }

    public function export()
    {
        return Excel::download(new HistoryExport(), 'REPORT.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeImport(Request $request)
    {
        $file = $request->file('file');
        // dd($file);
        Excel::import(new HistoryImport, $file);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateParameter(Request $request, $id)
    {
        $parameter = Parameter::where('fishpond_id', $id)->first();
        $parameter->update([
            $request->type => $request->standar
        ]);

        return redirect('/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
