<?php

namespace App\Exports;

use App\Models\History;
use App\Models\DetailHistory;
use Carbon\Carbon;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;

class HistoryExport implements Fromcollection, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;
    public function headings():array
    {
        return [
            'Tanggal',
            'Nama Tambak',
            'Lokasi',
            'Jam',
            'PH',
            'Suhu',
            'Dissolved Oxygen (DO)',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];
        $id = $_POST['id_fishpond'];
        if ($endDate != null && $startDate != null) {
            $history = History::where('fishpond_id', $id)->whereBetween('date', [Carbon::parse($startDate)->format('Y-m-d'), Carbon::parse($endDate)->format('Y-m-d')])->get();
        } else {
            $history = History::where('fishpond_id', $id)->get();
        }

        $detailHistory = [];
        foreach ($history as $data) {
            $details = DetailHistory::where('history_id', $data->id)->get();
            foreach ($details as $data2) {
                # code...
                array_push($detailHistory, $data2);
            }
        }

        $detailHistory = collect($detailHistory);

        return $detailHistory;
    }

    public function map($detailHistory):array
    {
        $date = $detailHistory->history->date;
        $name = $detailHistory->history->fishpond->name;
        $location = $detailHistory->history->fishpond->site->location . ", " . $detailHistory->history->fishpond->site->city;
        $time = $detailHistory->time;
        $ph = $detailHistory->ph;
        $suhu = $detailHistory->suhu;
        $oxygen = $detailHistory->oxygen;

        return [
            $date,
            $name,
            $location,
            $time,
            $ph,
            $suhu,
            $oxygen,
        ];
    }
}
