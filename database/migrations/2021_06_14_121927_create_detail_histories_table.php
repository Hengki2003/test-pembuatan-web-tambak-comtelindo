<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_histories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('history_id')->unsigned();
            $table->foreign('history_id')->references('id')->on('histories')->onDelete('cascade');
            $table->string('time');
            $table->integer('ph');
            $table->integer('suhu');
            $table->integer('oxygen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_histories');
    }
}
