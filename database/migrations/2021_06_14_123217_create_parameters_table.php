<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('fishpond_id')->unsigned();
            $table->foreign('fishpond_id')->references('id')->on('fishponds')->onDelete('cascade');
            $table->string('ph')->default('7.2-8.8');
            $table->string('suhu')->default('26-30');
            $table->string('oxygen')->default('3.7-6.2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameters');
    }
}
