@extends('layouts.app')
@section('style')
    <style>
        td{
            border: none !important;
        }
        /* datatable */

    .dataTables_wrapper .dataTables_paginate .paginate_button {
        min-width: 35px;
        width: auto !important;
        height: 35px;
        border-radius: 5px;
        background-color: #f1f1f1;
        vertical-align: top;
        color: #7E7E7E!important;
        margin: 0 2px;
        border: 0!important;
        line-height: 21px;
        box-shadow: none!important;
    }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}"> --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
@endsection

@section('content')
<div class="notika-status-area mb-5">
    <div class="container">
        <div class="d-flex" style="overflow-x: auto">
            @foreach ($fishponds as $data)
                <div class="ms-3" style="min-width: 300px">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <a href="/{{$data->id}}" class="mt-3" style="@if($id == $data->id)
                                color: rgb(0,194,146) !important;
                            @endif font-size: 20px">{{$data->name . " " . $data->site->city}}</a>
                        </div>
                        <div class="sparkline-bar-stats1">9,4,8,6,5,6,4,8,3,5,9,5</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Table Riwayat</h2>
                                    <p>Tabel yang menampikan riwayat dari status tambak setiap hari</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <form action="/{{$id}}/history/export" method="POST">
                                    @csrf
                                <button data-toggle="tooltip" data-placement="left" title="Download Report" class="btn"><i class="notika-icon notika-sent"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcomb area End-->
<!-- Data Table area Start-->
<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="basic-tb-hd ps-5 pe-5 pt-5">
                        <h2>Riwayat Status Tambak</h2><br>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Import
                        </button>


                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12" style="margin-top:20px">
                                <div class="nk-int-st">
                                    <input type="hidden" name="id_fishpond" value="{{$id}}">
                                    <label for="tanggal1" class="form-label">Tanggal Awal</label>
                                    <input type="date" id="tanggal1" name="startDate" class="form-control input-sm @error('date') is-invalid @enderror">
                                    @error('date')
                                        <div class="invalid-feedback" style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12" style="margin-top:20px">
                                <div class="nk-int-st">
                                    <label for="tanggal2" class="form-label">Tanggal Akhir</label>
                                    <input type="date" id="tanggal2" name="endDate" class="form-control input-sm @error('date') is-invalid @enderror">
                                    @error('date')
                                        <div class="invalid-feedback" style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </form>
                            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2" style="margin-top: 55px">
                                <div class="nk-int-st" style="margin-left: 93%;">
                                    <button id="dateButton" class="btn btn-success btn-blue notika-btn-success waves-effect"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive ps-5 pe-5 pt-3">
                        <table id="table-history" class="table table-striped dataTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>PH</th>
                                    <th>Suhu</th>
                                    <th>Dissolved Oksigen (DO)</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/history/import" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" class="form-control" name="file">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
    $(document).ready(function(){
        var historyTable = $('#table-history').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            ajax: {
                type: "GET",
                url: "/{{$id}}/history",
                data: {tanggal1 : "{{$tanggal1}}", tanggal2 : "{{$tanggal2}}"},
            },
            columns: [
            { data: 'DT_RowIndex', orderable: false, searchable:false, },
            { data: 'name', name: 'name', },
            { data: 'date', name: 'date', },
            { data: 'ph', name: 'ph', },
            { data: 'suhu', name: 'suhu', },
            { data: 'oxygen', name: 'oxygen', },
            { data: 'action', name: 'action', },
            ],
        });

        $('#dateButton').click(function() {
            var tanggal1 = $('#tanggal1').val();
            var tanggal2 = $('#tanggal2').val();

            historyTable.destroy()

            historyTable = $('#table-history').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                responsive: true,
                ajax: {
                    type: "GET",
                    url: "/{{$id}}/history",
                    data: {
                        data1 : tanggal1,
                        data2 : tanggal2
                    },
                },
                columns: [
                { data: 'DT_RowIndex', orderable: false, searchable:false, },
                { data: 'name', name: 'name', },
                { data: 'date', name: 'date', },
                { data: 'ph', name: 'ph', },
                { data: 'suhu', name: 'suhu', },
                { data: 'oxygen', name: 'oxygen', },
                { data: 'action', name: 'action', },
                ],
            });
        });
    });
</script>

@endsection
