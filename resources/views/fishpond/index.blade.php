@extends('layouts.app')

@section('grafik')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Jam', 'PH', 'Suhu', 'Oksigen'],
      ['01', 7, 30, 12],
      ['02', 8, 25, 10],
      ['03', 4, 26, 9],
      ['04', 5, 27, 11]
    ]);

    var options = {
      title: 'Company Performance',
      hAxis: {title: 'Jam',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
</script>
@endsection

@section('content')

<!-- Main Menu area End-->
<!-- Start Status area -->
<div class="notika-status-area">
    <div class="container">
        <div class="d-flex" style="overflow-x: auto">
            @foreach ($fishponds as $pond)
                <div class="ms-3" style="min-width: 300px">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <a href="/{{$pond->id}}" class="mt-3" style="font-size: 20px">{{$pond->name . " " . $pond->site->city}}</a>
                        </div>
                        <div class="sparkline-bar-stats1">9,4,8,6,5,6,4,8,3,5,9,5</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- End Status area-->
<!-- Start Sale Statistic area-->
<div class="notika-status-area mb-5">
    <div class="container pt-5" style="background-color: white; margin-top: 50px;">
        <div class="curved-inner-pro ms-5" style="top: 100px;">
            <div class="curved-ctn mb-5">
                <h2>Selamat datang di website pemantauan tambak</h2>
                <h6>Silahkan pilih tambak yang ingin dipantau</h6>
                <br><br><br><br><br><br><br><br><br><br><br><br>
            </div>
        </div>

    </div>
</div>
<!-- End Sale Statistic area-->

@endsection
