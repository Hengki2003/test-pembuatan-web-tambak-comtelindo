@extends('layouts.app')

@section('grafik')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});

    google.charts.setOnLoadCallback(phChart);
    google.charts.setOnLoadCallback(suhuChart);
    google.charts.setOnLoadCallback(oxygenChart);


    function phChart() {
        <?php
            $strParameter = explode('-', $parameter->ph);
        ?>
        // abs(intval($detail->ph) - intval($parameter->ph))
        var data = google.visualization.arrayToDataTable([
            ['Jam', 'PH', { role: 'style' }],
            @foreach ($detailsHistory as $detail)
            [
                '{{$detail->time}}',
                {{$detail->ph}},
                @if ($detail->ph <= (float) $strParameter[1] && $detail->ph >= (float) $strParameter[0])
                    'Green'
                @else
                    'Red'
                @endif
            ],
            @endforeach
            ]);

        var options = {
        title: 'PH Tambak',
        hAxis: {title: 'Jam',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0, maxValue: 14}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_ph'));
        chart.draw(data, options);
    }

    function suhuChart() {
        <?php
            $strParameter = explode('-', $parameter->suhu);
        ?>
        var data = google.visualization.arrayToDataTable([
            ['Jam', 'Suhu', { role: 'style' }],
            @foreach ($detailsHistory as $detail)
            ['{{$detail->time}}', {{$detail->suhu}},
                @if ($detail->suhu <= (float) $strParameter[1] && $detail->suhu >= (float) $strParameter[0])
                    'Green'
                @else
                    'Red'
                @endif
            ],
            @endforeach
            ]);

        var options = {
        title: 'Suhu Tambak (°C)',
        hAxis: {title: 'Jam',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0, maxValue: 40}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_suhu'));
        chart.draw(data, options);
    }

    function oxygenChart() {
        <?php
            $strParameter = explode('-', $parameter->oxygen);
        ?>
        var data = google.visualization.arrayToDataTable([
            ['Jam', 'O2', { role: 'style' }],
            @foreach ($detailsHistory as $detail)
            ['{{$detail->time}}', {{$detail->oxygen}},
                @if ($detail->oxygen <= (float) $strParameter[1] && $detail->oxygen >= (float) $strParameter[0])
                    'Green'
                @else
                    'Red'
                @endif
            ],
            @endforeach
            ]);

        var options = {
        title: 'Dissolved Oksigen Tambak (epm)',
        hAxis: {title: 'Jam',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0, maxValue: 14}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_oxygen'));
        chart.draw(data, options);
    }
    </script>
@endsection

@section('content')
<!-- End Status area-->
<!-- Start Sale Statistic area-->
<div class="notika-status-area" style="margin-top: -50px;">
    <div class="container pt-5" style="background-color: white; margin-top: 50px;">
        <div class="curved-inner-pro ms-5" style="top: 100px;">
            <div class="row" style="width: 100%">
                <div class="curved-ctn col-lg-10">
                    <h2>Satistik {{$fishpond->name}}</h2>
                    <p>{{$fishpond->site->location}}, {{$fishpond->site->city}}</p>
                </div>

                <div class="col-lg-2">
                    <a href="/{{$history->fishpond_id}}/history" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <div class="row pb-5">
            <div class="col-lg-3 mt-5">
                <div class="card mt-5 ms-4" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">Standar PH Tambak</h5><br>
                        <input class="form-control" type="text" name="standar" value="{{$parameter->ph}}" disabled><br>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                @if($detailsHistory->count() > 0)
                <div id="chart_ph" style="width: 100%; height: 500px;"></div>
                @else
                <div>Tidak Ada Data Hari Ini</div>
                @endif
            </div>
        </div>
        <div class="row pb-5">
            <div class="col-lg-3">
                <div class="card mt-5 ms-4" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">Standar Suhu Tambak</h5><br>
                        <input class="form-control" type="text" name="standar" value="{{$parameter->suhu}}" disabled><br>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                @if($detailsHistory->count() > 0)
                <div id="chart_suhu" style="width: 100%; height: 500px;"></div>
                @else
                <div>Tidak Ada Data Hari Ini</div>
                @endif
            </div>
        </div>
        <div class="row pb-5">
            <div class="col-lg-3">
                <div class="card mt-5 ms-4" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">Standar Dissolved Oksigen Tambak</h5><br>
                        <input class="form-control" type="text" name="standar" value="{{$parameter->oxygen}}" disabled><br>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                @if($detailsHistory->count() > 0)
                <div id="chart_oxygen" style="width: 100%; height: 500px;"></div>
                @else
                <div>Tidak Ada Data Hari Ini</div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- End Sale Statistic area-->

@endsection
