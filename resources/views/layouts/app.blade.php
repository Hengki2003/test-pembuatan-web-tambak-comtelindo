<!DOCTYPE html>
<html>
    <head>
        @include('layouts.head')

        @yield('style')

        @yield('grafik')

        @include('layouts.head')
        <style>
            .dataTables_wrapper .dataTables_paginate .paginate_button {
                min-width: 35px;
                width: auto !important;
                height: 35px;
                border-radius: 5px;
                background-color: #f1f1f1;
                vertical-align: top;
                color: #7E7E7E!important;
                margin: 0 2px;
                border: 0!important;
                line-height: 21px;
                box-shadow: none!important;
            }
            .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:focus, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
                color: #fff!important;
            }
        </style>
    </head>
    <body>

        @include('layouts.header')

        @yield('content')

        @include('layouts.footer')

        @yield('script')
    </body>
</html>
