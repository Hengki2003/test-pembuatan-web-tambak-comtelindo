    <!-- Start Footer area-->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2021. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
<script src="{{asset('asset/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
    <!-- wow JS
		============================================ -->
    <script src="{{asset('asset/js/wow.min.js')}}"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="{{asset('asset/js/jquery-price-slider.js')}}"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="{{asset('asset/js/owl.carousel.min.js')}}"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="{{asset('asset/js/jquery.scrollUp.min.js')}}"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="{{asset('asset/js/meanmenu/jquery.meanmenu.js')}}"></script>
    <!-- counterup JS
		============================================ -->
    <script src="{{asset('asset/js/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('asset/js/counterup/waypoints.min.js')}}"></script>
    <script src="{{asset('asset/js/counterup/counterup-active.js')}}"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="{{asset('asset/js/scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="{{asset('asset/js/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('asset/js/sparkline/sparkline-active.js')}}"></script>
    <!-- flot JS
		============================================ -->
    <script src="{{asset('asset/js/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('asset/js/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('asset/js/flot/flot-active.js')}}"></script>
    <!-- knob JS
		============================================ -->
    <script src="{{asset('asset/js/knob/jquery.knob.js')}}"></script>
    <script src="{{asset('asset/js/knob/jquery.appear.js')}}"></script>
    <script src="{{asset('asset/js/knob/knob-active.js')}}"></script>
    <!--  Chat JS
		============================================ -->
    <script src="{{asset('asset/js/chat/jquery.chat.js')}}"></script>
    <!--  todo JS
		============================================ -->
    <script src="{{asset('asset/js/todo/jquery.todo.js')}}"></script>
	<!--  wave JS
		============================================ -->
    <script src="{{asset('asset/js/wave/waves.min.js')}}"></script>
    <script src="{{asset('asset/js/wave/wave-active.js')}}"></script>
    <!-- plugins JS
		============================================ -->
    <script src="{{asset('asset/js/plugins.js')}}"></script>
    <!-- Data Table JS
		============================================ -->
    <script src="{{asset('asset/js/data-table/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('asset/js/data-table/data-table-act.js')}}"></script>
    <!-- main JS
		============================================ -->
    <script src="{{asset('asset/js/main.js')}}"></script>
    <!-- bootstrap select JS
        ============================================ -->
    <script src="{{asset('asset/js/bootstrap-select/bootstrap-select.js')}}"></script>
    <!--  Dialog JS
    ============================================ -->
    <script src="{{asset('asset/js/dialog/sweetalert2.min.js')}}"></script>
    <script src="{{asset('asset/js/dialog/dialog-active.js')}}"></script>

</body>

</html>
