-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2021 at 04:30 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tambak`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_histories`
--

CREATE TABLE `detail_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `history_id` bigint(20) UNSIGNED NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ph` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suhu` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oxygen` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_histories`
--

INSERT INTO `detail_histories` (`id`, `history_id`, `time`, `ph`, `suhu`, `oxygen`, `created_at`, `updated_at`) VALUES
(1, 1, '07', '8.5', '28', '4', NULL, NULL),
(2, 1, '08', '8', '27', '4.8', NULL, NULL),
(3, 1, '09', '7.5', '26', '5.5', NULL, NULL),
(4, 1, '10', '7.9', '28', '5', NULL, NULL),
(5, 2, '07', '7.5', '28', '3.8', NULL, NULL),
(6, 2, '08', '9', '27', '3.8', NULL, NULL),
(7, 2, '09', '8', '29', '5', NULL, NULL),
(8, 2, '10', '8', '27', '5', NULL, NULL),
(9, 3, '07', '7.6', '27', '4.5', NULL, NULL),
(10, 3, '08', '8', '28', '5', NULL, NULL),
(11, 3, '09', '6', '28', '5', NULL, NULL),
(12, 3, '10', '6.5', '29', '5', NULL, NULL),
(13, 4, '07', '8', '29', '5', NULL, NULL),
(14, 4, '08', '7.6', '27', '6', NULL, NULL),
(15, 4, '09', '7.3', '30', '4', NULL, NULL),
(16, 4, '10', '7.9', '28', '5.5', NULL, NULL),
(17, 7, '01', '7.5', '28', '3.8', NULL, NULL),
(18, 7, '02', '9', '25', '6', NULL, NULL),
(19, 7, '03', '7', '30', '5', NULL, NULL),
(20, 7, '04', '7', '24', '4', NULL, NULL),
(21, 7, '05', '9', '27', '5', NULL, NULL),
(22, 7, '06', '10', '20', '4', NULL, NULL),
(23, 7, '07', '5', '26', '7', NULL, NULL),
(24, 8, '01', '7.5', '28', '3.8', NULL, NULL),
(25, 8, '02', '5', '22', '5.6', NULL, NULL),
(26, 8, '03', '7', '28', '6.6', NULL, NULL),
(27, 8, '04', '6', '24', '5', NULL, NULL),
(28, 8, '05', '7', '33', '5.5', NULL, NULL),
(29, 8, '06', '8', '28', '6', NULL, NULL),
(30, 8, '07', '8', '30', '5', NULL, NULL),
(31, 9, '01', '8', '29', '5', NULL, NULL),
(32, 9, '02', '7.9', '22', '5.6', NULL, NULL),
(33, 9, '03', '7', '28', '6.6', NULL, NULL),
(34, 9, '04', '6', '24', '5', NULL, NULL),
(35, 9, '05', '7', '33', '5.5', NULL, NULL),
(36, 9, '06', '8', '28', '6', NULL, NULL),
(37, 9, '07', '8', '30', '5', NULL, NULL),
(38, 10, '01', '7.5', '28', '5', NULL, NULL),
(39, 10, '02', '7.9', '38', '3.8', NULL, NULL),
(40, 10, '03', '7', '28', '6.6', NULL, NULL),
(41, 10, '04', '6', '24', '5', NULL, NULL),
(42, 10, '05', '7', '33', '5.5', NULL, NULL),
(43, 10, '06', '8', '28', '6', NULL, NULL),
(44, 10, '07', '8', '30', '5', NULL, NULL),
(46, 8, '08', '9', '27', '3.8', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(47, 8, '09', '8', '29', '5', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(48, 8, '10', '8', '27', '5', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(49, 11, '01', '7.5', '28', '3.8', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(50, 11, '02', '5', '22', '5.6', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(51, 11, '03', '7', '28', '6.6', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(52, 11, '04', '6', '24', '5', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(53, 11, '05', '7', '33', '5.5', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(54, 11, '06', '8', '28', '6', '2021-06-20 16:37:31', '2021-06-20 16:37:31'),
(55, 11, '07', '8', '30', '5', '2021-06-20 16:37:31', '2021-06-20 16:37:31');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fishponds`
--

CREATE TABLE `fishponds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `site_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fishponds`
--

INSERT INTO `fishponds` (`id`, `site_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tambak Lele ', NULL, NULL),
(2, 1, 'Tambak Mujair', NULL, NULL),
(3, 3, 'Tambak Nila', NULL, NULL),
(4, 3, 'Tambak Ikan Mas', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fishpond_id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `histories`
--

INSERT INTO `histories` (`id`, `fishpond_id`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-06-21', NULL, NULL),
(2, 2, '2021-06-21', NULL, NULL),
(3, 3, '2021-06-21', NULL, NULL),
(4, 4, '2021-06-21', NULL, NULL),
(7, 1, '2021-06-20', NULL, NULL),
(8, 2, '2021-06-20', NULL, NULL),
(9, 3, '2021-06-20', NULL, NULL),
(10, 4, '2021-06-20', NULL, NULL),
(11, 2, '2021-06-19', '2021-06-20 16:37:31', '2021-06-20 16:37:31');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_14_115821_create_sites_table', 1),
(5, '2021_06_14_120638_create_fishponds_table', 1),
(6, '2021_06_14_121851_create_histories_table', 1),
(7, '2021_06_14_121927_create_detail_histories_table', 2),
(8, '2021_06_14_123217_create_parameters_table', 3),
(9, '2014_10_12_200000_add_two_factor_columns_to_users_table', 4),
(10, '2019_12_14_000001_create_personal_access_tokens_table', 4),
(11, '2021_06_18_084239_create_sessions_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fishpond_id` bigint(20) UNSIGNED NOT NULL,
  `ph` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '7.2-8.8',
  `suhu` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '26-30',
  `oxygen` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.7-6.2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `fishpond_id`, `ph`, `suhu`, `oxygen`, `created_at`, `updated_at`) VALUES
(1, 1, '7.2-8.8', '26-30', '3.7-6.2', NULL, '2021-06-18 16:53:43'),
(2, 2, '7.2-8.8', '26-30', '3.7-6.2', NULL, NULL),
(3, 3, '7.2-8.8', '26-30', '3.7-6.2', NULL, NULL),
(4, 4, '7.2-8.8', '26-30', '3.7-6.2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('rbts7qbsH3ESctFeecjwQ6r4CmvW8tDjJ8JeinUP', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiTXR0bmdxMFZHOVI3ZmwzNDNZdmR5cnN2WXNEZGpseFVUYjRVTGdXcSI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjMxOiJodHRwOi8vMTI3LjAuMC4xOjgwMDAvMS9oaXN0b3J5Ijt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTtzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJEIzZHh4ZVM2UUFqSjlVdUdDdWJ3Yk9ScUZxSTkxRG1LY2IzWlZHeXNmbWRnNGpwTllncDI2Ijt9', 1624242262);

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `city`, `location`, `created_at`, `updated_at`) VALUES
(1, 'Balikpapan', 'Sungai Ampal', NULL, NULL),
(2, 'Balikpapan', 'Gunung Guntur', NULL, NULL),
(3, 'Samarinda', 'Sungai Mahakam', NULL, NULL),
(4, 'Samarinda', 'Perum Sempaja Samarinda Utara\r\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'Hengki Agung Prayoga', 'hengkiagng@gmail.com', NULL, '$2y$10$B3dxxeS6QAjJ9UuGCubwbORqFqI91DmKcb3ZVGysfmdg4jpNYgp26', NULL, NULL, NULL, NULL, NULL, '2021-06-19 19:19:22', '2021-06-19 19:19:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_histories`
--
ALTER TABLE `detail_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_histories_history_id_foreign` (`history_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fishponds`
--
ALTER TABLE `fishponds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fishponds_site_id_foreign` (`site_id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `histories_fishpond_id_foreign` (`fishpond_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fishpond_id` (`fishpond_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_histories`
--
ALTER TABLE `detail_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fishponds`
--
ALTER TABLE `fishponds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_histories`
--
ALTER TABLE `detail_histories`
  ADD CONSTRAINT `detail_histories_history_id_foreign` FOREIGN KEY (`history_id`) REFERENCES `histories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fishponds`
--
ALTER TABLE `fishponds`
  ADD CONSTRAINT `fishponds_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `histories`
--
ALTER TABLE `histories`
  ADD CONSTRAINT `histories_fishpond_id_foreign` FOREIGN KEY (`fishpond_id`) REFERENCES `fishponds` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `parameters`
--
ALTER TABLE `parameters`
  ADD CONSTRAINT `parameters_ibfk_1` FOREIGN KEY (`fishpond_id`) REFERENCES `fishponds` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
